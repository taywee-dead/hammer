#ObjectsR = skeleton.o jointnode.o body.o pointcloud.o world.o primitive.o sphere.o sphere.o
#ObjectsD = skeleton-d.o jointnode-d.o body-d.o pointcloud-d.o world-d.o primitive-d.o sphere-d.o sphere-d.o
ObjectsRelease = pointcloud.o primitive.o world.o sphere.o
ObjectsDebug = pointcloud-d.o primitive-d.o world-d.o sphere-d.o
allObjects = $(ObjectsRelease) $(ObjectsDebug)

Libraries = libhammer.so libhammer-d.so

CC = clang
CXX = clang++

CompileOptsGlobal = -c -fpic -Wall -Wextra -std=c++11 -I.
CompileOptsRelease = $(CompileOptsGlobal) -O3
CompileOptsDebug = $(CompileOptsGlobal) -g -O0

LinkerOptsGlobal = -Wall -Wextra -fpic -std=c++11
LinkerOptsRelease = $(LinkerOptsGlobal) -O3 -s -lrefmath
LinkerOptsDebug = $(LinkerOptsGlobal) -O0 -g -lrefmath-d

.PHONY : all clean debug docs install install-docs release shared static uninstall uninstall-docs

all : $(Libraries)

install :
	-mv -v -t /usr/lib $(Libraries)
	cp -v --parents -t /usr/include `find . -iname "*.hxx"` 

install-docs : uninstall-docs
	-mkdir -p /usr/share/doc/hammer
	mv -v ./latex ./html -t /usr/share/doc/hammer
	mv -v ./man/man3/*.gz -t /usr/share/man/man3

uninstall :
	-rm -r /usr/include/hammer
	-rm /usr/include/hammer.hxx /usr/lib/libhammer*.so

uninstall-docs :
	-rm -rv /usr/share/doc/hammer
	-rm -rv /usr/share/man/man3/hammer_*

clean :
	-rm -v $(allObjects) $(Libraries) 

docs : Doxyfile
	doxygen
	rm -r ./man/man3/_*_.3
	gzip -f ./man/man3/*.3

blueprint.svg : blueprint.dot
	dot ./blueprint.dot -Tsvg -o./blueprint.svg

libhammer.so : $(ObjectsRelease)
	$(CXX) -shared -o ./libhammer.so $(ObjectsRelease) $(LinkerOptsRelease)
	
libhammer-d.so : $(ObjectsDebug)
	$(CXX) -shared -o ./libhammer-d.so $(ObjectsDebug) $(LinkerOptsDebug)

primitive.o : hammer/primitive.hxx hammer/primitive.cxx
	$(CXX) $(CompileOptsRelease) -o ./primitive.o ./hammer/primitive.cxx

pointcloud.o : hammer/pointcloud.hxx hammer/pointcloud.cxx primitive.o
	$(CXX) $(CompileOptsRelease) -o ./pointcloud.o ./hammer/pointcloud.cxx

sphere.o : hammer/sphere.hxx hammer/sphere.cxx primitive.o
	$(CXX) $(CompileOptsRelease) -o ./sphere.o ./hammer/sphere.cxx

world.o : hammer/world.hxx hammer/world.cxx pointcloud.o
	$(CXX) $(CompileOptsRelease) -o ./world.o ./hammer/world.cxx

primitive-d.o : hammer/primitive.hxx hammer/primitive.cxx
	$(CXX) $(CompileOptsDebug) -o ./primitive-d.o ./hammer/primitive.cxx

pointcloud-d.o : hammer/pointcloud.hxx hammer/pointcloud.cxx primitive-d.o
	$(CXX) $(CompileOptsDebug) -o ./pointcloud-d.o ./hammer/pointcloud.cxx

sphere-d.o : hammer/sphere.hxx hammer/sphere.cxx primitive-d.o
	$(CXX) $(CompileOptsDebug) -o ./sphere-d.o ./hammer/sphere.cxx

world-d.o : hammer/world.hxx hammer/world.cxx pointcloud-d.o
	$(CXX) $(CompileOptsDebug) -o ./world-d.o ./hammer/world.cxx
