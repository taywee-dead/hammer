/*
 *  Lead Coder:     Taylor C. Richberger
 */

#ifndef JOINTNODE_HXX
#define JOINTNODE_HXX

#include <list>
#include <iostream>

#include <refmath/matrix.hxx>

namespace Hammer
{
    /**
     * \brief Class which represents a joint of a skeleton.
     *
     * Contains two matrixes indicating the positions of the joint for mutating vertex position.
     * One of the matrices indicates the jointMatrix, and the other is calculated by traversing
     * the tree and multiplying all of the joint matrices to find the final world position.
     * May have at most one parent joint and many child joints.
     * None of the joints may be garbage-collected except for the root, and only if it is not contained
     * in a Skeleton.
     */
    class JointNode
    {
        friend class Skeleton;

        private:
            JointNode* parent;
            std::list<JointNode*> children;

            unsigned int jointNumber;

            JointNode(const JointNode& node);
            JointNode& operator=(const JointNode& node);

        public:

            RefMath::Matrixf worldMatrix;
            RefMath::Matrixf jointMatrix;

            JointNode();

            /**
             * Allows the programmer to decide whether the matrices are by reference.
             * The two parameter values are typically identical.
             * See RefMath::Matrixf for details on reference.
             *
             * \param worldByReference decides the reference status of the world matrix
             * \param jointByReference decides the reference status of the joint matrix
             */
            JointNode(const bool& worldByReference, const bool& jointByReference);

            /**
             * \brief Move constructor.
             *
             * Because Copy constructors are forbidden, due to complications with a tree, a move
             * constructor is provided for use in certain structures, like vectors and other
             * array-based structures.
             *
             * \param node the node to be moved
             */
            JointNode(JointNode&& node) noexcept;

            /**
             * Destroys all of this node's children as well.  Be sure not to directly delete any
             * node that is not a root node, or there will be an invalid pointer left in the
             * parent.
             */
            virtual ~JointNode() noexcept;

            /**
             * \brief Get joint number that was set by SetJointNumber()
             *
             * This is provided entirely for convenience, in order to ease the process of arranging
             * matrices in an array.
             *
             * \return joint number
             */
            unsigned int GetJointNumber();

            /**
             * \brief Set joint number that may be retrieved by GetJointNumber()
             *
             * This is provided entirely for convenience, in order to ease the process of arranging
             * matrices in an array.
             * 
             * \param jointNumber the integer to assign to the joint
             */
            void SetJointNumber(const unsigned int& jointNumber);

            /**
             * \brief Attach a child node to this one
             *
             * Also will set the child's parent node to be this node, removing from a current
             * parent if present.
             *
             * \param child the joint to attach
             */
            void Attach(JointNode* child);

            /**
             * \brief Remove a child node from this one
             *
             * Also will set the child's parent node to be nullptr.
             * If the supplied node does not belong to this node, nothing is done.
             *
             * \param child the joint to remove
             */
            void Remove(JointNode* child);

            /**
             * \brief Remove a child node from this one and delete the child
             *
             * Also will delete the child's children.
             * If the supplied node does not belong to this node, nothing is done.
             *
             * \param child the joint to Delete
             */
            void Delete(JointNode* child);

            /**
             * \brief traverse down the tree, and perform relevant multiplications on all children.
             */
            void Process();

            /**
             * \brief traverse down the tree, and print data for this node and children.
             */
            void List();
    };
}
#endif
