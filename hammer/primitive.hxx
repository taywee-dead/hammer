
/*
 *  Lead Coder:     Taylor C. Richberger
 */

#ifndef PRIMITIVE_HXX
#define PRIMITIVE_HXX

#include <string>
#include <sstream>
#include <cstdio>
#include <fstream>
#include <iostream>

#include <refmath/matrix.hxx>

namespace Hammer
{
    /**
     * Primitive class, defines a base for all basic collision objects to pull.  This is used for any object
     * that pulls a support.
     */
    class Primitive
    {
        friend class World;

        protected:
            /**
             * Position relative to world.
             */
            RefMath::Matrixd position;

            /**
             * Rotation relative to world.
             */
            RefMath::Matrixd rotation;

        public:
            Primitive();
            
            virtual ~Primitive();

            /**
            * \brief Returns the farthest point (transformed) along a given direction
            *
            * The direction given is in Body space, and as such is transformed upon entry. The distance
            * used is also the distance from the origin of the body (this should probably be reworked to 
            * give absolute distance in world coordinates).
            * In theory, the direction should be transformed by multiplying the input direction by the 
            * inverse rotation matrix.  In this way, the rotation transformation only needs to be applied
            * once (to the direction vector) rather than to each tested vertex in order.  This is left to the
            * implementing class, in order to not do it where not necessary (or where it's actually harmful, such
            * as in AABBs).
            *
            * \param direction The direction given in body space
            *
            * \returns point farthest along direction
            */
            virtual RefMath::Matrixd GetSupportPoint(const RefMath::Matrixd& direction) = 0;

            /**
            * \brief Sets the transformation relative to the body
            *
            * Dissociates matrix into position and rotation matrices, bottom 4 elements are ignored.
            *
            * \param transformation new transformation matrix, expected to be 4x4
            */
            void SetTransformation(const RefMath::Matrixd& transformation);

            /**
            * \brief Sets the position relative to the body
            *
            * \param position new position matrix, expected to be 3x1
            */
            void SetPosition(const RefMath::Matrixd& position);

            /**
            * \brief Sets the position relative to the body
            *
            * \param position new position matrix, expected to be 3x1
            */
            void SetPosition(RefMath::Matrixd&& position);

            /**
            * \brief Sets the rotation relative to the body
            *
            * \param rotation new rotation matrix, expected to be 3x3
            */
            void SetRotation(const RefMath::Matrixd& rotation);

            /**
            * \brief Sets the rotation relative to the body
            *
            * \param rotation new rotation matrix, expected to be 3x3
            */
            void SetRotation(RefMath::Matrixd&& rotation);

            /**
            * \brief Gets the transformation matrix
            *
            * Builds the transformation matrix out of the rotation and position
            *
            * \returns transformation matrix, not a pointer, invokes the overhead of construction
            */
            RefMath::Matrixd GetTransformation();

            /**
            * \brief Gets the position matrix
            *
            * \returns pointer to the position matrix, be careful that you do not reference this after destruction
            */
            RefMath::Matrixd* GetPosition();

            /**
            * \brief Gets the rotation matrix
            *
            * \returns pointer to the rotation matrix, be careful that you do not reference this after destruction
            */
            RefMath::Matrixd* GetRotation();
    };
}
#endif
