/*
 *  Lead Coder:     Taylor C. Richberger
 */

#include "jointnode.hxx"

namespace Hammer
{
    JointNode::JointNode() : worldMatrix(true, 4, 4), jointMatrix(true, 4, 4)
    {
        parent = nullptr;
    }

    JointNode::JointNode(const bool& worldByReference, const bool& jointByReference) : worldMatrix(worldByReference, 4, 4), jointMatrix(jointByReference, 4, 4)
    {
        parent = nullptr;
    }

    JointNode::JointNode(JointNode&& node) noexcept : children(std::move(node.children)), worldMatrix(std::move(node.worldMatrix)), jointMatrix(std::move(node.jointMatrix))
    {
    }

    JointNode::~JointNode() noexcept
    {
        for (std::list<JointNode*>::iterator it = children.begin(); it != children.end(); it++)
        {
            delete (*it);
        }
        children.clear();
    }

    unsigned int JointNode::GetJointNumber()
    {
        return (jointNumber);
    }

    void JointNode::SetJointNumber(const unsigned int& jointNumber)
    {
        this->jointNumber = jointNumber;
    }

    void JointNode::Attach(JointNode* child)
    {
        if (child->parent)
        {
            child->parent->Remove(child);
        }
        children.push_back(child);
        child->parent = this;
    }

    void JointNode::Remove(JointNode* child)
    {
        for (std::list<JointNode*>::iterator it = children.begin(); it != children.end(); it++)
        {
            if ((*it) == child)
            {
                child->parent = nullptr;
                children.erase(it);
                break;
            }
        }
    }

    void JointNode::Delete(JointNode* child)
    {
        for (std::list<JointNode*>::iterator it = children.begin(); it != children.end(); it++)
        {
            if ((*it) == child)
            {
                delete (*it);
                children.erase(it);
                break;
            }
        }
    }

    void JointNode::Process()
    {
        if (parent)
        {
            worldMatrix = parent->worldMatrix * jointMatrix;
        } else
        {
            worldMatrix = jointMatrix;
        }

        for (std::list<JointNode*>::iterator it = children.begin(); it != children.end(); it++)
        {
            (*it)->Process();
        }
    }

    void JointNode::List()
    {
        std::cout << "For joint number " << jointNumber << ": " << std::endl;
        if (parent)
        {
            std::cout << "Parent is number " << parent->GetJointNumber() << std::endl;
        } else
        {
            std::cout << "No parent" << std::endl;
        }

        for (std::list<JointNode*>::iterator it = children.begin(); it != children.end(); it++)
        {
            std::cout << "Has number " << (*it)->GetJointNumber() << " As a child." << std::endl;
        }

        std::cout << "jointMatrix: " << std::endl << jointMatrix.ToString() << std::endl;
        std::cout << "worldMatrix: " << std::endl << worldMatrix.ToString() << std::endl;

        for (std::list<JointNode*>::iterator it = children.begin(); it != children.end(); it++)
        {
            (*it)->List();
        }
    }
}

