/*
 *  Lead Coder:     Taylor C. Richberger
 */

#include "body.hxx"

namespace Hammer
{
    Body::Body()
    {
        vertices = nullptr;
        normals = nullptr;
        position = nullptr;
    }

    Body::~Body()
    {
        Destroy();
    }

    void Body::Destroy()
    {
        if (vertices)
        {
            delete[] (vertices);
            vertices = nullptr;
        }
        if (normals)
        {
            delete[] (normals);
            normals = nullptr;
        }
        if (position)
        {
            delete (position);
            position = nullptr;
        }
    }

    void Body::SetVertices(GLfloat* vertices, GLuint count, GLuint size)
    {
        vertexCount = count;
        unsigned int rows, columns;
        bool reference;
        Matrixf::GetDefaults(rows, columns, reference);
        Matrixf::SetDefaults(size, 1, false);

        this->vertices = new Matrixf[count];

        for (unsigned int i = 0; i < count; i++)
        {
            this->vertices[i].Set(&vertices[size * i]);
        }

        Matrixf::SetDefaults(rows, columns, reference);
    }

    void Body::SetNormals(GLfloat* normals, GLuint count, GLuint size)
    {
        normalCount = count;
        unsigned int rows, columns;
        bool reference;
        Matrixf::GetDefaults(rows, columns, reference);
        Matrixf::SetDefaults(size, 1, false);

        this->normals = new Matrixf[count];

        for (unsigned int i = 0; i < count; i++)
        {
            this->normals[i].Set(&normals[size * i]);
        }

        Matrixf::SetDefaults(rows, columns, reference);
    }

    void Body::CreatePosition(GLuint size, GLfloat* coords)
    {
        position = new Matrixf(false, size, 1);
        if (coords)
        {
            position->Set(coords);
        }

    }

    Matrixf* Body::GetPosition()
    {
        return (position);
    }
}
