
/*
 *  Lead Coder:     Taylor C. Richberger
 */

#ifndef WORLD_HXX
#define WORLD_HXX

#include <list>
#include <iostream>

#include <refmath/matrix.hxx>
#include <hammer/primitive.hxx>

namespace Hammer
{
    class World
    {
        protected:
            Primitive* player;
            Primitive* other;
        
        public:
            /**
             * \brief sets null pointers
             */
            World();
            
            /**
             * \brief Calls Destroy()
             */
            virtual ~World();
            
            void SetPlayer(Primitive* player);
            void SetOther(Primitive* other);

            static RefMath::Matrixd GetSupportPoint(Primitive* a, Primitive* b, RefMath::Matrixd& direction);
            static bool DoSimplex(std::list<RefMath::Matrixd>& supports, RefMath::Matrixd& direction);
            static bool CheckCollision(Primitive* a, Primitive* b);
    };
}
#endif
