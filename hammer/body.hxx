
/*
 *  Lead Coder:     Taylor C. Richberger
 */

#ifndef BODY_HXX
#define BODY_HXX

#include <string>
#include <sstream>
#include <cstdio>
#include <fstream>
#include <iostream>

#include <refmath/matrixf.hxx>

namespace Hammer
{
    class Body
    {
        protected:
            RefMath::Matrixd position;
            RefMath::Matrixd rotation;

        public:
            Body();
            
            virtual ~Body();

            Matrixf* GetPosition();

    };
}
#endif
