
/*
 *  Lead Coder:     Taylor C. Richberger
 */

#ifndef POINTCLOUD_HXX
#define POINTCLOUD_HXX

#include <string>
#include <sstream>
#include <cstdio>
#include <fstream>
#include <iostream>

#include <hammer/primitive.hxx>
#include <refmath/matrix.hxx>

namespace Hammer
{
    /**
     * \brief A physics model handling class
     */
    class PointCloud : public Primitive
    {
        protected:
            RefMath::Matrixd* vertices;

            unsigned int vertexCount;


        public:
            /**
             * \brief sets null pointers
             */
            PointCloud();
            
            /**
             * \brief Calls Destroy()
             */
            virtual ~PointCloud();

            virtual RefMath::Matrixd GetSupportPoint(const RefMath::Matrixd& direction);

            /**
             * \brief Destroys vertex array
             */
            void DestroyVertices();

            /**
             * \brief Sets vertices
             *
             * Sets vertex locations.
             *
             * \param vertices pointer to array of vertices
             * \param count number of vertices (not size of the array, but actual number of vertex coordinate sets)
             * \param size number of coordinates that defines a single vertex location
             */
            void SetVertices(double* vertices, unsigned int count, unsigned int size);
    };
}
#endif
