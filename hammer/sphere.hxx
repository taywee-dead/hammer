
/*
 *  Lead Coder:     Taylor C. Richberger
 */

#ifndef SPHERE_HXX
#define SPHERE_HXX

#include <string>
#include <sstream>
#include <cstdio>
#include <fstream>
#include <iostream>

#include <hammer/primitive.hxx>
#include <refmath/matrix.hxx>

namespace Hammer
{
    /**
     * Simple sphere collision class
     */
    class Sphere : public Primitive
    {
        protected:
            double radius;

            unsigned int vertexCount;


        public:
            /**
             * \brief sets null pointers
             */
            Sphere();
            
            /**
             * \brief Calls Destroy()
             */
            virtual ~Sphere();

            /**
            * \brief Returns the farthest distance along a given direction
            *
            * \param direction The direction given in body space
            *
            * \returns distance along direction
            */
            virtual RefMath::Matrixd GetSupportPoint(const RefMath::Matrixd& direction);

            void SetRadius(const double& radius);

            double GetRadius();
    };
}
#endif
