/*
 *  Lead Coder:     Taylor C. Richberger
 */

#include "world.hxx"

namespace Hammer
{
    World::World()
    {
    }

    World::~World()
    {
    }

    void World::SetPlayer(Primitive* player)
    {
        this->player = player;
    }

    void World::SetOther(Primitive* other)
    {
        this->other = other;
    }

    RefMath::Matrixd World::GetSupportPoint(Primitive* a, Primitive* b, RefMath::Matrixd& direction)
    {
        return (a->GetSupportPoint(direction) - b->GetSupportPoint(-direction));
    }

    bool World::DoSimplex(std::list<RefMath::Matrixd>& supports, RefMath::Matrixd& direction)
    {
        switch (supports.size())
        {
            case 2:
            {
                RefMath::Matrixd* a = &supports.back();
                RefMath::Matrixd* b = &supports.front();
                RefMath::Matrixd ab(*b - *a);
                // Because the origin is 0,0,0 by definition, a to the origin is just -a
                RefMath::Matrixd ao(-(*a));
                if (ab.Dot(ao) > 0.0)
                {
                    direction = ab.Cross(ao).Cross(ab);
                    return (false);
                } else
                {
                    direction = std::move(ao);
                    supports.pop_front();
                    return (false);
                }
                break;
            }

            case 3:
            {
                RefMath::Matrixd* a = &supports.back();
                std::list<RefMath::Matrixd>::iterator b = (++supports.begin());
                RefMath::Matrixd* c = &supports.front();

                RefMath::Matrixd ab(*b - *a);
                RefMath::Matrixd ac(*c - *a);
                // Because the origin is 0,0,0 by definition, a to the origin is just -a
                RefMath::Matrixd ao(-(*a));
                RefMath::Matrixd abc(ab.Cross(ac));

                RefMath::Matrixd abcxac(abc.Cross(ac));
                if (abcxac.Dot(ao) > 0.0)
                {
                    if (ac.Dot(ao) > 0.0)
                    {
                        supports.erase(b);
                        direction = ac.Cross(ao).Cross(ac);
                        return (false);
                    } else
                    {
                        if (ab.Dot(ao) > 0.0)
                        {
                            supports.pop_front();
                            direction = ab.Cross(ao).Cross(ab);
                            return (false);
                        } else
                        {
                            supports.pop_front();
                            supports.pop_front();
                            direction = std::move(ao);
                            return (false);
                        }
                    }
                } else
                {
                    RefMath::Matrixd abxabc(ab.Cross(abc));
                    if (abxabc.Dot(ao) > 0.0)
                    {
                        if (ab.Dot(ao) > 0.0)
                        {
                            supports.pop_front();
                            direction = ab.Cross(ao).Cross(ab);
                            return (false);
                        } else
                        {
                            supports.pop_front();
                            supports.pop_front();
                            direction = std::move(ao);
                            return (false);
                        }
                    } else
                    {
                        if (abc.Dot(ao) > 0.0)
                        {
                            direction = std::move(abc);
                            return (false);
                        } else
                        {
                            std::iter_swap(b, supports.begin());
                            abc *= -1.0;
                            direction = std::move(abc);
                            return (false);
                        }
                    }
                }

                break;
            }

            case 4:
            {
                RefMath::Matrixd* d = &supports.front();
                std::list<RefMath::Matrixd>::iterator b = (++supports.begin());
                // Sets c equal to b's previous element, and increments b, so that the order is d,c,b,a
                std::list<RefMath::Matrixd>::iterator c = b++;
                RefMath::Matrixd* a = &supports.back();

                RefMath::Matrixd ab(*b - *a);
                RefMath::Matrixd ac(*c - *a);
                RefMath::Matrixd ad(*d - *a);
                // Because the origin is 0,0,0 by definition, a to the origin is just -a
                RefMath::Matrixd ao(-(*a));
                RefMath::Matrixd abc(ab.Cross(ac));

                // Currently, simply sees which triangle is closest and then runs the triangle case.  This can likely be optimized,
                // so it will change in the future
                if (abc.Dot(ao) > 0.0)
                {
                    supports.pop_front();
                    return (DoSimplex(supports, direction));
                } else
                {
                    RefMath::Matrixd acd(ac.Cross(ad));
                    if (acd.Dot(ao) > 0.0)
                    {
                        supports.erase(b);
                        return (DoSimplex(supports, direction));
                    } else
                    {
                        RefMath::Matrixd adb(ad.Cross(ab));
                        if (adb.Dot(ao) > 0.0)
                        {
                            supports.erase(c);
                            std::iter_swap(b, supports.begin());
                            return (DoSimplex(supports, direction));
                        } else
                        {
                            return (true);
                        }
                    }
                }
                break;
            }

            default:
            {
                std::cerr << "Your DoSimplex recieved a support list with " << supports.size() << " points!  This is horribly bad!" << std::endl;
                break;
            }
        }

        return (false);
    }

    bool World::CheckCollision(Primitive* a, Primitive* b)
    {
        static const double startDir[3] = {1.0, 0.0, 0.0};
        RefMath::Matrixd direction(startDir, 3, 1);
        std::list<RefMath::Matrixd> supports;
        supports.push_back(GetSupportPoint(a, b, direction));
        direction = -(supports.back());
        while (true)
        {
            RefMath::Matrixd c(GetSupportPoint(a, b, direction));
            if (c.Dot(direction) < 0)
            {
                return (false);
            } else
            {
                supports.push_back(std::move(c));
                if (DoSimplex(supports, direction))
                {
                    return (true);
                }
            }
        }
    }
}
