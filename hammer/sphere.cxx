/*
 *  Lead Coder:     Taylor C. Richberger
 */

#include "sphere.hxx"

namespace Hammer
{
    Sphere::Sphere()
    {
    }

    Sphere::~Sphere()
    {
    }

    void Sphere::SetRadius(const double& radius)
    {
        this->radius = radius;
    }

    double Sphere::GetRadius()
    {
        return (radius);
    }

    RefMath::Matrixd Sphere::GetSupportPoint(const RefMath::Matrixd& direction)
    {
        return (position + (direction * radius));
    }

}
