/*
 *  Lead Coder:     Taylor C. Richberger
 */

#include "pointcloud.hxx"

namespace Hammer
{
    PointCloud::PointCloud()
    {
        vertices = nullptr;
    }

    PointCloud::~PointCloud()
    {
        DestroyVertices();
    }

    void PointCloud::DestroyVertices()
    {
        delete[] (vertices);
        vertices = nullptr;
    }

    void PointCloud::SetVertices(double* vertices, unsigned int count, unsigned int size)
    {
        vertexCount = count;
        unsigned int rows, columns;
        bool reference;
        RefMath::Matrixd::GetDefaults(rows, columns, reference);
        RefMath::Matrixd::SetDefaults(size, 1, false);

        this->vertices = new RefMath::Matrixd[count];

        for (unsigned int i = 0; i < count; i++)
        {
            this->vertices[i].Set(&vertices[size * i]);
        }

        RefMath::Matrixd::SetDefaults(rows, columns, reference);
    }

    RefMath::Matrixd PointCloud::GetSupportPoint(const RefMath::Matrixd& direction)
    {
        double distance = 0.0;
        double temp = 0.0;
        RefMath::Matrixd* supportPoint;
        for (unsigned int i = 0; i < vertexCount; i++)
        {
            if ((temp = vertices[i].Dot(direction)) > distance)
            {
                distance = temp;
                supportPoint = (vertices + i);
            }
        }
        return (*supportPoint + position);
    }
}
