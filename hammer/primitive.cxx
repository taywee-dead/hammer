/*
 *  Lead Coder:     Taylor C. Richberger
 */

#include "primitive.hxx"

namespace Hammer
{
    Primitive::Primitive() : position(false, 3, 1), rotation(false, 3, 3)
    {
    }

    Primitive::~Primitive()
    {
    }

    void Primitive::SetTransformation(const RefMath::Matrixd& transformation)
    {
        rotation.matrix[0] = transformation.matrix[0];
        rotation.matrix[1] = transformation.matrix[1];
        rotation.matrix[2] = transformation.matrix[2];

        rotation.matrix[3] = transformation.matrix[4];
        rotation.matrix[4] = transformation.matrix[5];
        rotation.matrix[5] = transformation.matrix[6];

        rotation.matrix[6] = transformation.matrix[8];
        rotation.matrix[7] = transformation.matrix[9];
        rotation.matrix[8] = transformation.matrix[10];

        position.matrix[0] = transformation.matrix[12];
        position.matrix[1] = transformation.matrix[13];
        position.matrix[2] = transformation.matrix[14];
    }

    void Primitive::SetPosition(const RefMath::Matrixd& position)
    {
        this->position = position;
    }

    void Primitive::SetPosition(RefMath::Matrixd&& position)
    {
        this->position = position;
    }

    void Primitive::SetRotation(const RefMath::Matrixd& rotation)
    {
        this->rotation = rotation;
    }

    void Primitive::SetRotation(RefMath::Matrixd&& rotation)
    {
        this->rotation = rotation;
    }

    RefMath::Matrixd Primitive::GetTransformation()
    {
        RefMath::Matrixd transformation(false, 4, 4);
        transformation.matrix[0] = rotation.matrix[0];
        transformation.matrix[1] = rotation.matrix[1];
        transformation.matrix[2] = rotation.matrix[2];
        transformation.matrix[3] = 0.0;

        transformation.matrix[4] = rotation.matrix[3];
        transformation.matrix[5] = rotation.matrix[4];
        transformation.matrix[6] = rotation.matrix[5];
        transformation.matrix[7] = 0.0;

        transformation.matrix[8] = rotation.matrix[6];
        transformation.matrix[9] = rotation.matrix[7];
        transformation.matrix[10] = rotation.matrix[8];
        transformation.matrix[11] = 0.0;

        transformation.matrix[12] = position.matrix[0];
        transformation.matrix[13] = position.matrix[1];
        transformation.matrix[14] = position.matrix[2];
        transformation.matrix[15] = 1.0;

        return (std::move(transformation));
    }

    RefMath::Matrixd* Primitive::GetPosition()
    {
        return (&position);
    }

    RefMath::Matrixd* Primitive::GetRotation()
    {
        return (&rotation);
    }
}
