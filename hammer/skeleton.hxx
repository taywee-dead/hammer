/*
 *  Lead Coder:     Taylor C. Richberger
 */

#ifndef SKELETON_HXX
#define SKELETON_HXX

#include <list>
#include <iostream>

#include <collider/jointnode.hxx>

namespace Hammer
{
    /**
     * \brief Class which represents a skeleton.
     *
     * Contains the root of one or more JointNode trees, none of which may be garbage-collected.
     */
    class Skeleton
    {
        private:
            std::list<JointNode*> children;
            Skeleton(const Skeleton& skeleton);
            Skeleton& operator=(const Skeleton& skeleton);

        public:

            /**
             * Does nothing.
             */
            Skeleton();

            /**
             * Sets the root of the JointNode tree on construction.
             *
             * \param child the new root
             */
            Skeleton(JointNode* child);

            /**
             * Creates a skeleton from another skeleton, only for moving.
             *
             * \param skeleton skeleton to be moved
             * \returns child the new root
             */
            Skeleton(Skeleton&& skeleton) noexcept;

            /**
             * Destroys the entire structure of the skeleton and all JointNode trees contained
             * within.
             */
            virtual ~Skeleton() noexcept;

            /**
             * \brief Attach a child root node to this one.
             *
             * Will also nullify the child's parent node.
             *
             * \param child the joint to attach
             */
            void Attach(JointNode* child);

            /**
             * \brief Remove a child root node from this one.
             *
             * Will also nullify the child's parent node.
             *
             * \param child the joint to remove
             */
            void Remove(JointNode* child);

            /**
             * \brief Delete a child tree.
             *
             * \param child the root of the tree to remove
             */
            void Delete(JointNode* child);

            /**
             * Calls the Process() function of each root node.
             */
            void Process();

            /**
             * Traverse down all trees, and print data for this node and children.
             */
            void List();
    };
}
#endif
