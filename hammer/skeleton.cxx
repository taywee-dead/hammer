/*
 *  Lead Coder:     Taylor C. Richberger
 */

#include "skeleton.hxx"

namespace Hammer
{
    Skeleton::Skeleton()
    {
    }

    Skeleton::Skeleton(JointNode* child)
    {
        Attach(child);
    }

    Skeleton::Skeleton(Skeleton&& skeleton) noexcept : children(std::move(skeleton.children))
    {
    }

    Skeleton::~Skeleton() noexcept
    {
        for (std::list<JointNode*>::iterator it = children.begin(); it != children.end(); it++)
        {
            delete (*it);
        }
        children.clear();
    }

    void Skeleton::Attach(JointNode* child)
    {
        if (child->parent)
        {
            child->parent->Remove(child);
        }
        children.push_back(child);
        child->parent = nullptr;
    }

    void Skeleton::Remove(JointNode* child)
    {
        for (std::list<JointNode*>::iterator it = children.begin(); it != children.end(); it++)
        {
            if ((*it) == child)
            {
                child->parent = nullptr;
                children.erase(it);
                break;
            }
        }
    }

    void Skeleton::Delete(JointNode* child)
    {
        for (std::list<JointNode*>::iterator it = children.begin(); it != children.end(); it++)
        {
            if ((*it) == child)
            {
                delete (*it);
                children.erase(it);
                break;
            }
        }
    }

    void Skeleton::Process()
    {
        for (std::list<JointNode*>::iterator it = children.begin(); it != children.end(); it++)
        {
            (*it)->Process();
        }
    }

    void Skeleton::List()
    {
        std::cout << "***In skeleton structure referenced by " << this << ":" << std::endl;
        for (std::list<JointNode*>::iterator it = children.begin(); it != children.end(); it++)
        {
            (*it)->List();
        }
    }
}

